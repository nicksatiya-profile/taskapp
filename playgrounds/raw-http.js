'use strict'

const http = require('http');

const url = 'http://api.weatherstack.com/current?access_key=d9cd19e0e17cb59a3dacc320824bb220&query=45,-75'


const request = http.request(url, (response) => {

    let data = '';

    response.on('data', (chunk) => {
        data += chunk.toString();
        console.log(chunk);
    });

    response.on('end', () => {
        const body = JSON.parse(data);
        console.log(body);
    });
})

request.on('error', (error) => {
    console.log(error);
});

request.end();