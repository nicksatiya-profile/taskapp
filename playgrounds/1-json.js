'use strict'

const fs = require('fs');

// const book =
// {
//     title: 'Ego is the enemy',
//     author: 'Ryan Holiday'
// }

// const bookJSON = JSON.stringify(book);

// fs.writeFileSync('1-json.json', bookJSON);

// const dataBuffer = fs.readFileSync('1-json.json');
// const dataJSON = dataBuffer.toString();
// const data = JSON.parse(dataJSON);

// console.log(data.title);

const dataBuffer = fs.readFileSync('1-json.json');
const dataJSON = dataBuffer.toString();
let data = JSON.parse(dataJSON);

console.log(data);


data.name = 'Nick';
data.age = 25;

// data = {
//     name: 'Nick',
//     age: 25
// };

console.log(data);

const stringJSON = JSON.stringify(data);

fs.writeFileSync('1-json.json', stringJSON);


