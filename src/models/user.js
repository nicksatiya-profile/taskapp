const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
    name: {
        type: String
    },
    age: {
        type: Number,
        // Custom Validation
        validate(value) {
            if (value < 0) {
                throw new Error('Age must be a positive number');
            }
        }
    },
    email: {
        type: String,
        unique: true,
        required: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid');
            }
        }
    },
    password: {
        type: String,
        required: true,
        trim: true,
        minlength: 7,
        validate(value) {
            if (value.toLowerCase().includes('password')) {
                throw new Error('Cannot use password as your password!');
            }


        }
    },
});

userSchema.statics.findbyCredentials = async (email, password) => {
    const user = await User.findOne({ email });

    if (!user) {
        throw new Error('Unable to login');
    }
    else {
        const isMatch = await bcrypt.compare(password, user.password);

        if (!isMatch) {
            throw new Error('Unable to login');
        }

        return user;
    }
}

// Middleware for mongoose BEFORE saving
// Uses a NORMAL function. Arrow functions don't work here
// since we are using 'this' keyword
/* Hashing the plain text password */
userSchema.pre('save', async function (next) {
    const user = this;

    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8); // 8 is a good number of times to iterate
    }

    next(); // Always call next or else the code will hang
});

const User = mongoose.model('User', userSchema);

module.exports = User;