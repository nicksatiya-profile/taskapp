const express = require('express');
const router = new express.Router();
const Task = require('../models/task');

// Resource Creation Endpoints

router.post('/task', async (req, res) => {
    const task = new Task(req.body);

    try {
        await task.save();
        res.status(201).send(task);
    } catch (error) {
        res.status(500).send(error); // Error 500 is an internal server error
    }

});

// Resource Reading Endpoints

router.get('/task', async (req, res) => {

    try {
        const tasks = await Task.find({});

        if (!tasks) {
            res.status(500).send();
        }
        else {
            res.send(tasks);
        }
    } catch (error) {
        res.status(500).send();
    }

});


router.get('/task/:id', async (req, res) => {
    const _id = req.params.id;

    try {
        const task = await Task.findById(_id);

        if (!task) {
            res.status(404).send();
        }
        else {
            res.send(task);
        }
    } catch (error) {
        res.status(500).send();
    }
});


// Resource Updating Endpoint

router.patch('/task/:id', async (req, res) => {

    const updates = Object.keys(req.body);
    const allowUpdates = ['description', 'completed'];
    const isValidOperation = updates.every((update) => allowUpdates.includes(update));

    if (!isValidOperation) {
        return res.status(404).send({ error: 'Invalid task property to update!' });
    }

    try {

        const task = await Task.findById(req.params.id);

        updates.forEach((update) => task[update] = req.body[update]);

        await task.save();

        // const task = await Task.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators: true });

        if (!task) {
            res.status(404).send();
        }
        else {
            res.send(task);
        }
    } catch (error) {
        res.status(500).send();
    }
});


// Resource Deleting Endpoints

router.delete('/task/:id', async (req, res) => {
    try {
        const task = await Task.findByIdAndDelete(req.params.id);

        if (!task) {
            res.status(404).send();
        }
        else {
            res.send(task);
        }
    } catch (error) {
        res.status(500).send();
    }
});

module.exports = router;