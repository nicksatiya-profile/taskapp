const express = require('express');
const router = new express.Router();
const User = require('../models/user');


// Resource Creation Endpoints

router.post('/users', async (req, res) => {
    const user = new User(req.body);

    try {
        await user.save();
        res.status(201).send(user);

    } catch (error) {
        res.status(400).send(error); // Since a good response comes at the 200s,
        // if an error occurs from the user, we should send a 400 error.
        // This command forces an error 400 to be sent
    }

});

router.post('/users/login', async (req, res) => {
    try {
        const user = await User.findbyCredentials(req.body.email, req.body.password);
        res.send(user);

    } catch (error) {
        res.status(400).send();
    }
});


// Resource Reading Endpoints

router.get('/users', async (req, res) => {

    try {
        const users = await User.find({});
        res.send(users);

    } catch (error) {
        res.status(400).send(error);
    }

});

router.get('/users/:id', async (req, res) => {
    const _id = req.params.id;

    try {
        const user = await User.findById(_id);

        if (!user) {
            res.status(404).send();
        }
        else {
            res.send(user);
        }

    } catch (error) {
        res.status(500).send();
    }

});

// Resource Updating Endpoint
router.patch('/users/:id', async (req, res) => {

    // In the case that a user tries to update something that he shouldn't,
    // we will do a check to see if what the user passed in is an updatable param

    const updates = Object.keys(req.body); // Return enumerable properties as strings in array
    const allowUpdates = ['name', 'age', 'email', 'password'];
    // Returns true for every match and false when there is a mismatch
    const isValidOperation = updates.every((update) => allowUpdates.includes(update));

    // Do not bother going through the try-catch, check first if the property given by
    // the user to update is a parameter that we allowed
    if (!isValidOperation) {
        return res.status(404).send({ error: 'Invalid property to update!' });
    }

    try {

        // Mongoose uses some functions that bypass the Middleware
        // So when adding middle ware we need to use the code below instead
        // In this case we manually update the read user object and
        // then manually save it.

        const user = await User.findById(req.params.id);

        updates.forEach((update) => user[update] = req.body[update]);

        await user.save();

        // Below is the code when no middleware is used.

        // const user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators: true });

        if (!user) {
            res.status(404).send();
        }
        else {
            res.send(user);
        }
    } catch (error) {
        // The one thing that could go wrong is that the ID we
        // received wasn't valid for our model. For eg. user gave
        // us a /users/{blank}. This is against our model and would
        // be a bad request
        res.status(400).send(error); // 400 - bad request
    }
});

// Resource Deleting Endpoints
router.delete('/users/:id', async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);

        if (!user) {
            res.status(404).send();
        }
        else {
            res.send(user);
        }
    } catch (error) {
        res.status(500).send();
    }
});

module.exports = router;